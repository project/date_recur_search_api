# Recurring Dates Field Search API

This module provides Search API support for fields created with the Recurring
Dates Field module.

The module defines an additional content entity datasource for each recurring
date field. This datasource should be used instead of the datasource for content
entities that comes with Search API module.

If your index has a mixture of bundles with a recurring date field and bundles
without, it is fine to mix this module's datasources with the SearchAPI
datasources for the bundles without the field.

The recurring date field datasource generates multiple items for each occurrence
of a recurring date field, up to a configurable point in the future. Each
occurrence will appear as a different search result.

When search items are loaded, the occurrence date is set on a computed field on
the entity. This field should be used for filtering, sorting, and displaying
results instead of the recurring date field.

So for example, if an entity's recurring date field is set to recur on the 1st
of the month, and the datasource is configured to generate occurrences for 2
years into the future, this will generate 24 search items, and this will be
further multiplied by each translation.

## Requirements

This module requires the following modules:

- [Recurring Dates Field](https://www.drupal.org/project/date_recur)
- [Search API](https://www.drupal.org/project/search_api)
- [Computed Field](https://www.drupal.org/project/computed_field)

## Recommended modules

- [Search API Common Fields](https://www.drupal.org/project/search_api_common_field):
  This allows common fields from Content datasources and Date Occurrence
  datasources.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Edit or create a SearchAPI index.
2. Enable the 'Date occurrences' datasource for the entity type and recurring
   date you want to index.
3. Add the computed date occurrence field to the index to sort items by date.
4. Configure the view mode used in search results to shoe the computed date
   occurrence field.

## Known issues and limitations

- Date recur fields are assumed to *not* be translatable.
- SearchAPI's implementation of hook_node_access_records_alter() will not work
  with this module's indexed items, because one entity has multiple items in the
  index. To correctly support the node access system, the hook should be
  implemented in this module with the same logic.
