<?php

namespace Drupal\date_recur_search_api\EventSubscriber;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\date_recur_search_api\Plugin\Deriver\DateRecurDatasourceDeriver;
use Drupal\date_recur\Event\DateRecurEvents;
use Drupal\date_recur\Event\DateRecurValueEvent;
use Drupal\search_api\IndexInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Updates the SearchAPI tracker when date recur occurrences change.
 */
class SearchApiTrackerSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current primary database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Creates a SearchApiTrackerSubscriber instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The current primary database.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * Reacts to the FIELD_VALUE_SAVE event.
   *
   * @param \Drupal\date_recur\Event\DateRecurValueEvent $event
   *   The date recur event.
   */
  public function onFieldValueSave(DateRecurValueEvent $event): void {
    $this->iterateIndexesForEvent($event, function (IndexInterface $index, string $datasource_plugin_id, array $existing_tracked_ids, array $current_tracked_ids) {
      $deleted_ids = array_diff($existing_tracked_ids, $current_tracked_ids);
      $new_ids = array_diff($current_tracked_ids, $existing_tracked_ids);
      $updated_ids = array_intersect($existing_tracked_ids, $current_tracked_ids);

      if (!empty($deleted_ids)) {
        $index->trackItemsDeleted($datasource_plugin_id, $deleted_ids);
      }
      if (!empty($new_ids)) {
        $index->trackItemsInserted($datasource_plugin_id, $new_ids);
      }
      if (!empty($updated_ids)) {
        $index->trackItemsUpdated($datasource_plugin_id, $updated_ids);
      }
    });
  }

  /**
   * Reacts to the FIELD_ENTITY_DELETE event.
   *
   * @param \Drupal\date_recur\Event\DateRecurValueEvent $event
   *   The date recur event.
   */
  public function onFieldEntityDelete(DateRecurValueEvent $event): void {
    $this->iterateIndexesForEvent($event, function (IndexInterface $index, string $datasource_plugin_id, array $existing_tracked_ids, array $current_tracked_ids) {
      $index->trackItemsDeleted($datasource_plugin_id, $existing_tracked_ids);
    });
  }

  /**
   * Iterate the indexes for a given event.
   *
   * @param \Drupal\date_recur\Event\DateRecurValueEvent $event
   *   The date recur event.
   * @param callable $callable
   *   A callable to invoke with every applicable index.
   */
  protected function iterateIndexesForEvent(DateRecurValueEvent $event, callable $callable) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $date_recur_field_item_list */
    $date_recur_field_item_list = $event->getField();
    $field_storage_definition = $date_recur_field_item_list->getFieldDefinition()->getFieldStorageDefinition();

    $indexes = $this->getIndexesForFieldStorageDefinition($field_storage_definition);

    if (empty($indexes)) {
      return;
    }

    $datasource_plugin_id = $this->getFullyQualifiedDatasourcePluginId($field_storage_definition);
    foreach ($indexes as $index) {
      /** @var \Drupal\date_recur_search_api\Plugin\search_api\datasource\DateRecur $datasource */
      $datasource = $index->getDatasource($datasource_plugin_id);

      $existing_tracked_ids = $this->getAllTrackedIds($index, $date_recur_field_item_list->getEntity(), $field_storage_definition);
      $current_tracked_ids = $datasource->generateEntityItemIds($date_recur_field_item_list->getEntity(), $field_storage_definition->getName());

      $callable($index, $datasource_plugin_id, $existing_tracked_ids, $current_tracked_ids);
    }
  }

  /**
   * Get a list of all IDs in the tracker for a given entity.
   *
   * The tracker has no public API for understanding which IDs are being
   * tracked. When an entity is resaved with new occurrences, we don't have a
   * list of previous occurrences that were generated and indexed. Generating a
   * list like this would be problematic, since occurrences are relative to a
   * non-fixed start date and may extend into a variable amount of time into the
   * future. In order to make sure the tracker completely replaces old
   * occurrences with the new, we need to understand which items are tracked for
   * a given entity.
   *
   * This requires querying the tracker table directly. This is not ideal
   * because the tracker instances is pluggable, one backed by a table only
   * happens to be the default. Luckily there does not seem to be many
   * alternative trackers, meaning it's very likely this integration will be
   * compatible with most SAPI installations.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index to query.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to get tracked occurrences for.
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $fieldStorage
   *   The storage definition of the field occurrances are generated for.
   *
   * @return array
   *   An array of tracker IDs.
   */
  protected function getAllTrackedIds(IndexInterface $index, EntityInterface $entity, FieldStorageDefinitionInterface $fieldStorage): array {
    $datasource_plugin_prefix = $this->getFullyQualifiedDatasourcePluginId($fieldStorage) . '/';

    $select = $this->database->select('search_api_item', 'sai');
    $select->condition('index_id', $index->id());
    $select->fields('sai', ['item_id']);
    $select->condition('item_id', $this->database->escapeLike(sprintf('%s%s:', $datasource_plugin_prefix, $entity->id())) . '%', 'LIKE');
    $results = $select->execute();

    return array_map(function ($tracker_id) use ($datasource_plugin_prefix) {
      return substr($tracker_id, strlen($datasource_plugin_prefix));
    }, array_keys($results->fetchAllAssoc('item_id')));
  }

  /**
   * Retrieves all indexes that are configured to index the date recur field.
   *
   * @return \Drupal\search_api\IndexInterface[]
   *   All indexes that are configured to index the given field (using the
   *   date_recur datasource plugin).
   */
  public function getIndexesForFieldStorageDefinition(FieldStorageDefinitionInterface $field_storage_definition): array {
    /** @var \Drupal\search_api\IndexInterface[] $indexes */
    $indexes = $this->entityTypeManager->getStorage('search_api_index')->loadMultiple();
    $datasource_id = $this->getFullyQualifiedDatasourcePluginId($field_storage_definition);
    return array_filter($indexes, function (IndexInterface $index) use ($datasource_id) {
      return $index->isValidDatasource($datasource_id);
    });
  }

  /**
   * Lookup a datasource ID for a given field storage.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $fieldStorageDefinition
   *   The field storage.
   *
   * @return string
   *   The datasource ID for a given field storage.
   */
  protected static function getFullyQualifiedDatasourcePluginId(FieldStorageDefinitionInterface $field_storage_definition): string {
    return sprintf('date_recur:%s', DateRecurDatasourceDeriver::getDatasourceDerivativeId($field_storage_definition->getTargetEntityTypeId(), $field_storage_definition->getName()));
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Dispatched after an entity containing a date recur field is saved.
    $events[DateRecurEvents::FIELD_VALUE_SAVE] = ['onFieldValueSave'];
    // Dispatched when an entity containing date recur fields is almost deleted.
    $events[DateRecurEvents::FIELD_ENTITY_DELETE] = ['onFieldEntityDelete'];
    return $events;
  }

}
