<?php

namespace Drupal\date_recur_search_api\Plugin\ComputedField;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\computed_field\Attribute\ComputedField;
use Drupal\computed_field\Field\ComputedFieldDefinitionWithValuePluginInterface;
use Drupal\computed_field\Plugin\ComputedField\ComputedFieldBase;

/**
 * Provides a computed field alongside every date_recur field.
 *
 * This doesn't actually compute anything, but exists for our SearchAPI
 * datasource to set a date occurrence value on.
 *
 * We don't implement getCacheability() because this value can be cached with
 * the rest of the entity. See
 * date_recur_search_api_entity_build_defaults_alter().
 *
 * @todo Figure out if it's possible to hide this in view modes other than those
 * for SearchAPI.
 */
#[ComputedField(
  id: "date_recur_date_occurrence",
  label: new TranslatableMarkup("Date occurrence"),
  field_type: "daterange",
  no_ui: TRUE,
  attach: [
    'scope' => 'bundle',
    'dynamic' => TRUE,
  ],
)]
class DateOccurrence extends ComputedFieldBase {

  /**
   * The suffix used when forming the field name from the date_recur field.
   */
  public const COMPUTED_FIELD_SUFFIX = '_occurrence';

  /**
   * {@inheritdoc}
   */
  public function attachAsBundleField(&$fields, EntityTypeInterface $entity_type, string $bundle): void {
    /** @var \Drupal\Core\Field\FieldDefinitionInterface $field */
    foreach ($fields as $field) {
      if ($field->getType() == 'date_recur') {
        $derived_computed_field = $this->createComputedFieldDefinition($entity_type, $bundle);

        $derived_computed_field_name = $field->getName() . self::COMPUTED_FIELD_SUFFIX;
        // Override the name, as createComputedFieldDefinition() will have set a
        // dummy value.
        $derived_computed_field->setName($derived_computed_field_name);

        $fields[$derived_computed_field_name] = $derived_computed_field;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldName(): ?string {
    // Gets replaced in attachAsBundleField().
    return 'dummy';
  }

  /**
   * {@inheritdoc}
   */
  public function computeValue(EntityInterface $host_entity, ComputedFieldDefinitionWithValuePluginInterface $computed_field_definition): array {
    // We don't compute a value. It is set by our SearchAPI datasource plugin
    // when search items are loaded.
    return [];
  }

}
