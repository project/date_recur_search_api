<?php

namespace Drupal\date_recur_search_api\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derives a datasource plugin for each date recur field.
 *
 * Borrowed from date_recur_sapi module.
 *
 * @see \Drupal\date_recur_search_api\Plugin\search_api\datasource\DateRecur
 */
class DateRecurDatasourceDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * DateRecurOccurrencesDeriver constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeManager->getDefinitions() as $entityType) {
      if (!$entityType->entityClassImplements(FieldableEntityInterface::class)) {
        continue;
      }
      // Getting the pre-generated field map runs into a circular dependency
      // issue with solr, which has a data type plugin deriver. Using the active
      // field storage definitions side-steps this conflict.
      $fields = $this->entityFieldManager->getActiveFieldStorageDefinitions($entityType->id());
      foreach ($fields as $field) {
        if ($field->getType() === 'date_recur') {
          $this->derivatives[static::getDatasourceDerivativeId($entityType->id(), $field->getName())] = [
            'label' => $this->t('Date occurrences: @entity_type (@field_name)', [
              '@entity_type' => $entityType->getLabel(),
              '@field_name' => $field->getName(),
            ]),
            'description' => $this->t('Index the occurrences of a date for a given entity type and field as individual documents in the search index.'),
            'field_name' => $field->getName(),
            'entity_type' => $entityType->id(),
          ] + $base_plugin_definition;
        }
      }
    }

    return $this->derivatives;
  }

  /**
   * Get the datasource plugin derivative ID.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param string $fieldName
   *   The field name.
   *
   * @return string
   *   The datasource derivative ID.
   */
  public static function getDatasourceDerivativeId(string $entityTypeId, string $fieldName): string {
    return sprintf('%s__%s', $entityTypeId, $fieldName);
  }

}
