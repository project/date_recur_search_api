<?php

namespace Drupal\date_recur_search_api\Plugin\search_api\datasource;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\date_recur_search_api\Plugin\ComputedField\DateOccurrence;
use Drupal\date_recur\DateRange;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\search_api\Plugin\search_api\datasource\ContentEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Datasource for occurrences of a recurring date.
 *
 * This generates multiple items for each occurrence of a recurring date field,
 * for a fixed period into the future. So for example, if an entity's recurring
 * date field is set to recur on the 1st of the month, and the datasource is
 * configured with a pre-create period of 2 years, this will generate 24 search
 * items, and each occurrence will appear as a different search result.
 *
 * Furthermore, if the entity is translated with two languages, this will
 * generate 48 items. It is assumed that recurring date fields are NOT
 * translatable.
 *
 * When search items are loaded, the occurrence date is set on a computed field
 * on the entity. This field should be used for filtering, sorting, and display
 * in results instead of the recurring date field.
 *
 * Item ids are formed from the following parts joined with a ':':
 *  - Entity ID
 *  - Langcode of the translation
 *  - Field delta of the recurring date field
 *  - Occurrence ID. This consists of the start and end dates of the occurence
 *    joined with '--'.
 *
 * This datasource extends the existing ContentEntity datasource in order to
 * make use of several features including the rendering of the underlying entity
 * and the access checking.
 *
 * This borrows a lot from the sandbox project at
 * https://www.drupal.org/sandbox/sam/3200275.
 *
 * @see \Drupal\date_recur_search_api\Plugin\ComputedField\DateOccurrence
 *
 * @SearchApiDatasource(
 *   id = "date_recur",
 *   label = @Translation("Date occurrences"),
 *   deriver = "Drupal\date_recur_search_api\Plugin\Deriver\DateRecurDatasourceDeriver"
 * )
 */
class DateRecur extends ContentEntity {

  /**
   * The date format to use for forming occurrence IDs.
   */
  const OCCURRENCE_DELTA_DATETIME_FORMAT = 'Y-m-d-H-i-s';

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $datasource = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $datasource->entityFieldManager = $container->get('entity_field.manager');

    return $datasource;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();

    // How far into the future we should create occurrences should be indexed.
    $default_configuration['pre_create'] = 'P2Y';

    return $default_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['pre_create'] = [
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => $this->t('Pre-create period for occurrences'),
      '#description' => $this->t("How far into the future the index should create date occurrences for a recurring date. Enter a PHP DateInterval string."),
      '#default_value' => $this->configuration['pre_create'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $pre_create = $form_state->getValue('pre_create');

    try {
      new \DateInterval($pre_create);
    }
    catch (\Exception $e) {
      $form_state->setError($form['pre_create'], $this->t('The pre-create period value must be a <a href=":url">valid PHP DateInterval string</a>.', [
        ':url' => 'https://www.php.net/manual/en/dateinterval.construct.php',
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getItemId(ComplexDataInterface $item) {
    // We can't return anything useful here.
    // See https://www.drupal.org/project/search_api/issues/3447818.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemIds($page = NULL) {
    /** @var $entity_type \Drupal\Core\Entity\EntityTypeInterface */
    $entity_type = $this->getEntityType();
    $entity_type_id = $entity_type->id();

    $storage = $this->getEntityStorage();
    // We must start with an entity query to build tracking information, since
    // the date_recur table tracks all revision IDs, we cannot know which
    // records from that table are associated with the default revision of any
    // given entity when querying for them directly.
    // @todo Optimise this query by using a direct DB query if possible: see the
    // parent class.
    $entities_query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->exists($this->getFieldName());

    if ($this->hasBundles()) {
      $enabled_bundles = array_keys($this->getBundles());
      if (count($enabled_bundles) < count($this->getEntityBundles())) {
        $bundle_property = $entity_type->getKey('bundle');
        $entities_query->condition($bundle_property, $enabled_bundles, 'IN');
      }
    }

    // The pager will increment as long as NULL isn't returned from this method.
    if ($page !== NULL) {
      $page_size = $this->getConfigValue('tracking_page_size');
      assert($page_size, 'Tracking page size is not set.');

      $entities_query->range($page * $page_size, $page_size);
    }
    $all_entities = $entities_query->execute();
    if (empty($all_entities)) {
      return NULL;
    }

    // For all loaded entities, compute all their item IDs. We may get multiple
    // IDs for different languages, and multiple IDs for date recur occurrences.
    $item_ids = [];

    $loaded_entities = $storage->loadMultiple($all_entities);
    foreach ($loaded_entities as $entity_id => $entity) {
      $item_ids = array_merge($item_ids, $this->generateEntityItemIds($entity, $this->getFieldName()));
    }

    return $item_ids;
  }

  /**
   * Gets a list of all search item IDs for the given entity's occurrences.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name to get occurrences from.
   *
   * @return array
   *   An array of all item ids for the date occurrences of this datasource's
   *   date recur field on the given entity. These IDs are formed from the
   *   following pieces, joined with a ':':
   *    - The entity ID
   *    - The langcode of the translation
   *    - The field item delta
   *    - The date occurrence delta, which itself consists of the start and end
   *      date joined with '--'.
   */
  public function generateEntityItemIds(ContentEntityInterface $entity, string $field_name): array {
    $pre_create_interval = new \DateInterval($this->configuration['pre_create']);
    $until = (new \DateTime('now'))->add($pre_create_interval);

    $enabled_languages = array_keys($this->getLanguages());
    $translations = array_keys($entity->getTranslationLanguages());
    $translations = array_intersect($translations, $enabled_languages);

    $item_ids = [];
    foreach ($entity->{$field_name} as $field_item_delta => $date_recur_item) {
      $item_occurrences = $date_recur_item->getHelper()->getOccurrences(NULL, $until);
      foreach ($item_occurrences as $item_occurrence) {
        $occurrence_delta_id = $this->buildOccurrenceDeltaIdentifier($item_occurrence);

        // Assume that the date_recur field is NOT translatable!
        // @todo Revisit this if there is a use case.
        // Load the occurrences and extract their identifiers.
        foreach ($translations as $langcode) {
          $item_ids[] = $entity->id() . ':' . $langcode . ':' . $field_item_delta . ':' . $occurrence_delta_id;
        }
      }
    }
    return $item_ids;
  }

  /**
   * Build an identifier for a date range occurrence.
   *
   * @param \Drupal\date_recur\DateRange $occurrence
   *   The occurrence.
   *
   * @return string
   *   The identifier.
   *
   * @see self::getDateRangeFromOccurrenceDeltaIdentifier()
   */
  protected function buildOccurrenceDeltaIdentifier(DateRange $occurrence): string {
    return implode('--', array_map(function (\DateTimeInterface $datetime) {
      $cloned = clone $datetime;
      $cloned->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      return $cloned->format(static::OCCURRENCE_DELTA_DATETIME_FORMAT);
    }, [$occurrence->getStart(), $occurrence->getEnd()]));
  }

  /**
   * Gets a date range object from an occurrence delta identifier.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity the field is on.
   * @param integer $field_delta
   *   The field delta for the date_recur field the occurrence is from.
   * @param string $occurrence_delta
   *   The date occurrence delta used in the Search API item ID.
   *
   * @return \Drupal\date_recur\DateRange
   *   A date range object for the start and end times of the occurrence, and
   *   the timezone set to the timezone of the date_recur field delta item.
   *
   * @see self::buildOccurrenceDeltaIdentifier()
   */
  protected function getDateRangeFromOccurrenceDeltaIdentifier(ContentEntityInterface $entity, int $field_delta, string $occurrence_delta): DateRange {
    [$start, $end] = explode('--', $occurrence_delta);

    // Get the timezone from the entity's date_recur field.
    $date_recur_field_timezone_name = $entity->get($this->getFieldName())[$field_delta]->timezone;
    $date_recur_field_timezone = new \DateTimeZone($date_recur_field_timezone_name);

    return new DateRange(
      \DateTime::createFromFormat(static::OCCURRENCE_DELTA_DATETIME_FORMAT, $start)->setTimezone($date_recur_field_timezone),
      \DateTime::createFromFormat(static::OCCURRENCE_DELTA_DATETIME_FORMAT, $end)->setTimezone($date_recur_field_timezone),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids) {
    // Assemble an array mapping string IDs to their sub-parts. Keys are item
    // IDs, values are arrays with properties:
    //  - entity_id: The ID of the entity this item is for.
    //  - langcode: The language of the translation.
    //  - field_delta: The delta of the date value used for the item.
    //  - occurrence_id: The occurrence ID of the date occurrence for the item.
    //    This is derived from the date range by
    //    self::buildOccurrenceDeltaIdentifier().
    $id_parts = array_map(function ($id) {
      $parts = explode(':', $id);
      return array_combine([
        'entity_id',
        'langcode',
        'field_delta',
        'occurrence_id',
      ], $parts);
    }, array_combine($ids, $ids));

    // Load all entities for the set of occurrences being indexed.
    $entities = $this->getEntityStorage()->loadMultiple(array_column($id_parts, 'entity_id'));

    $items = [];
    foreach ($ids as $id) {
      $entity_id = $id_parts[$id]['entity_id'];

      // Clone the entity so we get a different entity object for each item ID.
      // If we don't do this, we get the same PHP object, and every time we set
      // the occurrence date value on the field, it pollutes all previous items
      // which are the same entity.
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = clone($entities[$entity_id]);

      $entity = $entity->getTranslation($id_parts[$id]['langcode']);

      $date_range = $this->getDateRangeFromOccurrenceDeltaIdentifier($entity, $id_parts[$id]['field_delta'], $id_parts[$id]['occurrence_id']);

      $computed_field_name = $this->getComputedFieldName();

      // This is the fiddly part. We set the date occurrence value on the
      // computed field. This persists throughout all operations SearchAPI does
      // on this, because this method is SearchAPI's only way to get load a
      // search item.
      $entity->{$computed_field_name}->value = $date_range->getStart()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      $entity->{$computed_field_name}->end_value = $date_range->getEnd()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

      $items[$id] = $entity->getTypedData();
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityBundles() {
    if ($this->hasBundles()) {
      $bundles = $this->getEntityTypeBundleInfo()->getBundleInfo($this->getEntityTypeId());

      // Get the bundles which have our date_recur field on them.
      $date_recur_field_map = $this->entityFieldManager->getFieldMapByFieldType('date_recur');
      $bundle_with_field = $date_recur_field_map[$this->getEntityTypeId()][$this->getFieldName()]['bundles'];

      return array_intersect_key($bundles, $bundle_with_field);
    }
    else {
      return [];
    }
  }

  /**
   * Get the date recur field name.
   *
   * @return string
   *   The date recur field name.
   */
  protected function getFieldName(): string {
    return $this->pluginDefinition['field_name'];
  }

  /**
   * Get the computed field name.
   *
   * @return string
   *   The computed field name.
   */
  protected function getComputedFieldName(): string {
    return $this->pluginDefinition['field_name'] . DateOccurrence::COMPUTED_FIELD_SUFFIX;
  }

}
