<?php

namespace Drupal\Tests\date_recur_search_api\Kernel;

use Drupal\date_recur_search_api\Plugin\ComputedField\DateOccurrence;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\search_api\Entity\Index;

/**
 * Tests the date_recur SearchAPI datasource.
 *
 * @group date_recur_search_api
 */
class DateRecurDatasourceTest extends KernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'language',
    'field',
    'entity_test',
    'datetime',
    'datetime_range',
    'date_recur',
    'search_api',
    'search_api_test',
    'computed_field',
    'date_recur_search_api',
  ];

  /**
   * The search index used for testing.
   *
   * @var \Drupal\search_api\IndexInterface
   */
  protected $index;

  /**
   * The datasource used for testing.
   *
   * @var \Drupal\search_api\Plugin\search_api\datasource\ContentEntity
   */
  protected $datasource;

  /**
   * The name of the date_recur field.
   *
   * @var string
   */
  protected string $dateRecurFieldName;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    // Enable translation for the entity_test module.
    \Drupal::state()->set('entity_test.translation', TRUE);

    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test_with_bundle');
    $this->installConfig(['language']);
    $this->installConfig(['datetime_range']);
    $this->installConfig(['date_recur']);
    $this->installConfig('search_api');
    $this->installSchema('search_api', ['search_api_item']);
    $this->installEntitySchema('search_api_task');

    // Create bundles.
    $alpha = $this->entityTypeManager->getStorage('entity_test_bundle')->create([
      'id' => 'alpha',
      'label' => 'Alpha',
    ]);
    $alpha->save();
    $beta = $this->entityTypeManager->getStorage('entity_test_bundle')->create([
      'id' => 'beta',
      'label' => 'Beta',
    ]);
    $beta->save();

    // Create a date_recur field.
    $this->dateRecurFieldName = $this->randomMachineName();
    $field_storage = $this->entityTypeManager->getStorage('field_storage_config')->create([
      'field_name' => $this->dateRecurFieldName,
      'entity_type' => 'entity_test_with_bundle',
      'type' => 'date_recur',
    ]);
    $field_storage->save();
    $field = $this->entityTypeManager->getStorage('field_config')->create([
      'field_name' => $this->dateRecurFieldName,
      'entity_type' => 'entity_test_with_bundle',
      'bundle' => 'alpha',
      'label' => 'Label',
    ]);
    $field->save();

    // Create some languages.
    for ($i = 0; $i < 2; ++$i) {
      ConfigurableLanguage::create([
        'id' => 'l' . $i,
        'label' => 'language - ' . $i,
        'weight' => $i,
      ])->save();
    }
    $this->container->get('language_manager')->reset();

    $datasource_name = 'date_recur:' . 'entity_test_with_bundle' . '__' .  $this->dateRecurFieldName;

    // Create a test server.
    $server = $this->entityTypeManager->getStorage('search_api_server')->create([
      'name' => 'Test server',
      'id' => 'test',
      'status' => 1,
      'backend' => 'search_api_test',
    ]);
    $server->save();

    // Create a test index.
    $this->index = Index::create([
      'name' => 'Test Index',
      'id' => 'test_index',
      'status' => TRUE,
      'server' => $server->id(),
      'datasource_settings' => [
        $datasource_name => [],
      ],
      'tracker_settings' => [
        'default' => [],
      ],
    ]);
    $this->index->save();

    $this->datasource = $this->index->getDatasource($datasource_name);
  }

  /**
   * Tests indexing entities with the date_recur datasource.
   */
  public function testDateRecurDatasource() {
    $tracker = $this->index->getTrackerInstance();
    $this->assertEquals(0, $tracker->getTotalItemsCount());

    $entity = $this->entityTypeManager->getStorage('entity_test_with_bundle')->create([
      'type' => 'alpha',
      'langcode' => 'l0',
      $this->dateRecurFieldName => [
        'value' => '2014-06-01T12:00:00',
        'end_value' => '2014-06-01T13:00:00',
        'rrule' => 'FREQ=MONTHLY;INTERVAL=1',
        'timezone' => 'Australia/Sydney',
      ],
    ]);
    $entity->save();

    // The tracker now has items for the entity.
    // We don't check for the exact number of items, as getting a fixed number
    // for that would require the date set in the entity field value to depend
    // on the date this test is being run, which would then make assertions for
    // the date values dynamic and more complicated to debug.
    $this->assertNotEquals(0, $tracker->getTotalItemsCount());
    $tracked_item_count = $tracker->getTotalItemsCount();

    $item_ids = $this->datasource->getItemIds();

    // Multiple occurrences of the entity are in the index.
    $this->assertContains('1:l0:0:2014-06-01-12-00-00--2014-06-01-13-00-00', $item_ids);
    $this->assertContains('1:l0:0:2014-07-01-12-00-00--2014-07-01-13-00-00', $item_ids);

    $loaded_items = $this->datasource->loadMultiple(['1:l0:0:2014-06-01-12-00-00--2014-06-01-13-00-00', '1:l0:0:2014-07-01-12-00-00--2014-07-01-13-00-00']);

    $occurrence_field_name = $this->dateRecurFieldName . DateOccurrence::COMPUTED_FIELD_SUFFIX;

    // Entities retrieved from the index have their computed occurrence field
    // value set.
    $item = $loaded_items['1:l0:0:2014-06-01-12-00-00--2014-06-01-13-00-00'];
    $this->assertEquals('2014-06-01T12:00:00', $item->get($occurrence_field_name)->value);

    $item = $loaded_items['1:l0:0:2014-07-01-12-00-00--2014-07-01-13-00-00'];
    $this->assertEquals('2014-07-01T12:00:00', $item->get($occurrence_field_name)->value);

    // Change the RRULE to be every two months.
    $entity->get($this->dateRecurFieldName)->rrule = 'FREQ=MONTHLY;INTERVAL=2';
    $entity->save();

    // There should be fewer items being tracked now that the recurring date is
    // not as frequent.
    $this->assertLessThan($tracked_item_count, $tracker->getTotalItemsCount());

    // Deleting the entity deletes the items from the tracker.
    $entity->delete();
    $this->assertEquals(0, $tracker->getTotalItemsCount());
  }

}
